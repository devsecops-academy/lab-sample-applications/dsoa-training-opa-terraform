provider "aws" {
  region = "us-west-1"
  skip_credentials_validation = true
  skip_requesting_account_id  = true
  skip_metadata_api_check     = true
  s3_use_path_style           = true
  access_key                  = "mock_access_key"
  secret_key                  = "mock_secret_key"
}

resource "aws_s3_bucket" "privates3bucket" {
  bucket = "my-test-s3-private-bucket"
  acl = "private"
  versioning {
    enabled = true
  }
}

  resource "aws_s3_bucket" "publics3bucket" {
    bucket = "my-test-s3-public-bucket"
    acl = "public-read"
    versioning {
      enabled = true
    }
}

# Api gateway with "REGIONAL" end point start
resource "aws_api_gateway_stage" "testapigatewaystage" {
  stage_name    = "prod"
  rest_api_id   = "${aws_api_gateway_rest_api.testgatewayrestapi.id}"
  deployment_id = "${aws_api_gateway_deployment.testapigatewaydeployment.id}"
}

resource "aws_api_gateway_rest_api" "testgatewayrestapi" {
  name        = "MyDemoAPI"
  description = "This is my API for demonstration purposes"

  endpoint_configuration {
    types = ["REGIONAL"]
  }

}

resource "aws_api_gateway_deployment" "testapigatewaydeployment" {
  depends_on  = [aws_api_gateway_integration.testapigatewayintegration]
  rest_api_id = "${aws_api_gateway_rest_api.testgatewayrestapi.id}"
  stage_name  = "dev"
}

resource "aws_api_gateway_resource" "testapigetwayresource" {
  rest_api_id = "${aws_api_gateway_rest_api.testgatewayrestapi.id}"
  parent_id   = "${aws_api_gateway_rest_api.testgatewayrestapi.root_resource_id}"
  path_part   = "mytestresource"
}

resource "aws_api_gateway_method" "testapigetwaymethod" {
  rest_api_id   = "${aws_api_gateway_rest_api.testgatewayrestapi.id}"
  resource_id   = "${aws_api_gateway_resource.testapigetwayresource.id}"
  http_method   = "GET"
  authorization = "NONE"
}

resource "aws_api_gateway_method_settings" "s" {
  rest_api_id = "${aws_api_gateway_rest_api.testgatewayrestapi.id}"
  stage_name  = "${aws_api_gateway_stage.testapigatewaystage.stage_name}"
  method_path = "${aws_api_gateway_resource.testapigetwayresource.path_part}/${aws_api_gateway_method.testapigetwaymethod.http_method}"

  settings {
    metrics_enabled = true
    logging_level   = "INFO"
  }
}

resource "aws_api_gateway_integration" "testapigatewayintegration" {
  rest_api_id = "${aws_api_gateway_rest_api.testgatewayrestapi.id}"
  resource_id = "${aws_api_gateway_resource.testapigetwayresource.id}"
  http_method = "${aws_api_gateway_method.testapigetwaymethod.http_method}"
  type        = "MOCK"
}

# Api gateway with "REGIONAL" end point end


# Api gateway with default end point start
resource "aws_api_gateway_stage" "testapigatewaystagedefault" {
  stage_name    = "prod"
  rest_api_id   = "${aws_api_gateway_rest_api.testgatewayrestapidefault.id}"
  deployment_id = "${aws_api_gateway_deployment.testapigatewaydeploymentdefault.id}"
}

resource "aws_api_gateway_rest_api" "testgatewayrestapidefault" {
  name        = "MyDemoAPIdefault"
  description = "This is my API for demonstration purposes"

}

resource "aws_api_gateway_deployment" "testapigatewaydeploymentdefault" {
  depends_on  = [aws_api_gateway_integration.testapigatewayintegrationdefault]
  rest_api_id = "${aws_api_gateway_rest_api.testgatewayrestapidefault.id}"
  stage_name  = "dev"
}

resource "aws_api_gateway_resource" "testapigetwayresourcedefault" {
  rest_api_id = "${aws_api_gateway_rest_api.testgatewayrestapidefault.id}"
  parent_id   = "${aws_api_gateway_rest_api.testgatewayrestapidefault.root_resource_id}"
  path_part   = "mytestresource"
}

resource "aws_api_gateway_method" "testapigetwaymethoddefault" {
  rest_api_id   = "${aws_api_gateway_rest_api.testgatewayrestapidefault.id}"
  resource_id   = "${aws_api_gateway_resource.testapigetwayresourcedefault.id}"
  http_method   = "GET"
  authorization = "NONE"
}

resource "aws_api_gateway_method_settings" "sdefault" {
  rest_api_id = "${aws_api_gateway_rest_api.testgatewayrestapidefault.id}"
  stage_name  = "${aws_api_gateway_stage.testapigatewaystage.stage_name}"
  method_path = "${aws_api_gateway_resource.testapigetwayresourcedefault.path_part}/${aws_api_gateway_method.testapigetwaymethoddefault.http_method}"

  settings {
    metrics_enabled = true
    logging_level   = "INFO"
  }
}

resource "aws_api_gateway_integration" "testapigatewayintegrationdefault" {
  rest_api_id = "${aws_api_gateway_rest_api.testgatewayrestapidefault.id}"
  resource_id = "${aws_api_gateway_resource.testapigetwayresourcedefault.id}"
  http_method = "${aws_api_gateway_method.testapigetwaymethoddefault.http_method}"
  type        = "MOCK"
}
# Api gateway with default end point end
