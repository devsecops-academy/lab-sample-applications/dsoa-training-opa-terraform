package terraform.analysis

import input as tfplan


#########
# Policy
#########


authzs3bucket = false {
    count(public_s3_bucket) != 0
}

# check public s3 bucket
public_s3_bucket[resource_name] {
	s3buckets := tfplan[_]
    resource_name := s3buckets.address
    s3buckets.change.after.acl == "public-read"
}
